import os

from Core.IO.load import load_validation_config, load_index
from Core.IO.save import save_string_to_txt, save_to_json
from Validation.validation_methods import Validation

def run():
    root = os.path.abspath('./../')
    config = load_validation_config()
    setups = config['setups']

    for setup in setups:
        print('Start validation for: ' + setup['name'])
        name = setup['name']
        predicted_path = root + '/Resources/Model/' + name + '/predicted.xml'
        validation = Validation(setup['gold_path'], predicted_path, setup['gold_format'], setup['predicted_format'], setup['aspect_index_path'])
        conf_matrixes = validation.build_conf_matrix_per_aspect()
        aspect_score = validation.micro_avg_F1_score_apsects()
        aspect_sentiment_score, micro, macro, sentiment_conf_matrixes = validation.accuracy_aspect_sentiments()

        scores = {
            'aspect_score': aspect_score,
            'aspect_sentiment_score': aspect_sentiment_score
        }

        text = 'Aspect-Score: ' + str(aspect_score) + '\n'
        text += 'Aspect-Sentiment-Score: ' + str(aspect_sentiment_score) + '\n'
        text += 'Aspect-Sentiment-Score (Micro): ' + str(micro) + '\n'
        text += 'Aspect-Sentiment-Score (Macro): ' + str(macro) + '\n\n'
        text += conf_matrixes
        text += sentiment_conf_matrixes

        save_string_to_txt(text, root + '/Resources/Model/' + name + '/validation.txt')
        save_to_json(scores, root + '/Resources/Model/' + name + '/validation.json')

    overview(root)

def overview(root):
    model_path = root + '/Resources/Model/'
    setups = next(os.walk(model_path))[1]

    text = ''
    max = 0
    for setup in setups:
        if len(setup) > max:
            max = len(setup)


    for setup in setups:
        try:
            scores = load_index(model_path + setup + '/validation.json')
        except:
            continue
        num_tabs = int((max - len(setup)) / 4) + 1
        text += setup + '\t' * num_tabs + str(round(scores['aspect_score'], 4)) + '\t' + str(round(scores['aspect_sentiment_score'], 4)) + '\n'

    save_string_to_txt(text, model_path + 'overview.txt')




run()