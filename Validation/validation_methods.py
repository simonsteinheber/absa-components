import numpy as np
from sklearn.metrics import f1_score

from Core.DataLoading.DataProvider import DataProvider
from Core.IO.load import load_index

class Validation:

    def __init__(self, gold_path, predicted_path, gold_format, predicted_format, aspect_index_path):
        self.gold = DataProvider(gold_path, gold_format).get_data()
        self.predicted = DataProvider(predicted_path, predicted_format).get_data()
        self.aspect_index = load_index(aspect_index_path)

    def build_conf_matrix_per_aspect(self):
        gold_aspect_sentiments = self.gold.getAspectSentiments()
        predicted_aspect_sentiments = self.predicted.getAspectSentiments()

        gold_aspects = []
        for text in gold_aspect_sentiments:
            gold_aspects.append(list(np.array(text)[:, 0]))
        gold_aspects = np.array([self._translate_aspects(x) for x in gold_aspects])


        predicted_aspects = []
        for text in predicted_aspect_sentiments:
            predicted_aspects.append(list(np.array(text)[:, 0]))
        predicted_aspects = np.array([self._translate_aspects(x) for x in predicted_aspects])

        aspects = {}
        for asp in self.aspect_index:
            aspects[asp] = np.zeros((2,2))

        for i, gold in enumerate(gold_aspects):
            predicted = predicted_aspects[i]
            for asp in self.aspect_index:
                index = self.aspect_index[asp]
                #GOLD TRUE
                if gold[index] == 1:
                    #PRED TURE = TRUE POSITIVE
                    if predicted[index] == 1:
                        aspects[asp][0, 0] += 1
                    #PRED FALSE = FALSE NEGATIVE
                    else:
                        aspects[asp][0, 1] += 1
                #GOLD FALSE
                else:
                    #PRED TRUE = FALSE POSITIVE
                    if predicted[index] == 1:
                        aspects[asp][1, 0] += 1
                    #PRED FALSE = TRUE NEGATIVE
                    else:
                        aspects[asp][1, 1] += 1

        self.conf_matrixes = aspects
        return self._print_conf_matrixes()

    def micro_avg_F1_score_apsects(self):
        prec, rec = self._micro_avg_prec_rec()

        return 2 * prec * rec / (prec + rec)

    def _micro_avg_prec_rec(self):
        tp = 0
        fp = 0
        fn = 0
        for asp in self.conf_matrixes:
            matrix = self.conf_matrixes[asp]
            tp += matrix[0, 0]
            fp += matrix[1, 0]
            fn += matrix[0, 1]

        prec = tp / (tp + fp)
        rec = tp / (tp + fn)

        return prec, rec

    def accuracy_aspect_sentiments(self):
        gold_asp_sents = self.gold.getAspectSentiments()
        pred_asp_sents = self.predicted.getAspectSentiments()

        aspects = {}
        classes = self.build_class_index(gold_asp_sents)
        for asp in self.aspect_index:
            aspects[asp] = np.zeros((len(classes), len(classes)))

        for i, preds in enumerate(pred_asp_sents):
            golds = gold_asp_sents[i]
            for asp, sent in preds:
                index_pred = classes[sent]
                index_gold = 0
                for asp_gold, sent_gold in golds:
                    if asp == asp_gold:
                        index_gold = classes[sent_gold]
                aspects[asp][index_gold, index_pred] += 1

        text = self._print_sentiment_conf_matrixes(aspects)

        asp_accs = []
        summed_matrixes = np.zeros((len(classes), len(classes)))
        for asp in aspects:
            matrix = aspects[asp]
            diag = np.sum(np.diag(matrix))
            sum = np.sum(matrix)
            if sum == 0:
                asp_accs.append(0)
            else:
                asp_accs.append(diag/sum)
            summed_matrixes += matrix

        micro = np.sum(np.diag(summed_matrixes))/np.sum(summed_matrixes)
        macro = np.sum(asp_accs)/len(asp_accs)

        predicted_right = 0
        all_gold_labels = 0
        for i, preds in enumerate(pred_asp_sents):
            for pred in preds:
                if pred in gold_asp_sents[i]:
                    predicted_right += 1
            all_gold_labels += len(gold_asp_sents[i])


        return predicted_right/all_gold_labels, micro, macro, text

    def build_class_index(self, labels):
        i = 0
        new_labeles = []
        while i < len(labels):
            label = labels[i]
            if not label == []:
                new_labeles.append(label)
            i += 1
        labels = new_labeles

        labels = np.concatenate(labels)
        labels = labels[:, 1]
        classes = np.unique(labels, axis=0)

        result = {'N/A': 0}
        for i, _class in enumerate(classes):
            result[_class] = i + 1

        return result


    def _print_conf_matrixes(self):
        text = ''
        for asp in self.conf_matrixes:
            matrix = self.conf_matrixes[asp]
            matrix_text = self._print_conf_matrix(matrix)
            text += asp + ':\n'
            text += matrix_text + '\n\n'

        return text


    def _print_conf_matrix(self, matrix):
        text = '\t\t\tPRED Pos\tPRED Neg\n'
        placeholder = ''
        if matrix[0, 0] < 10:
            placeholder = ' '
        text += 'GOLD Pos \t' + str(matrix[0, 0]) + placeholder + '\t\t' + str(matrix[0, 1]) + '\n'

        placeholder = ''
        if matrix[1, 0] < 10:
            placeholder = ' '
        text += 'GOLD Neg \t' + str(matrix[1, 0]) + placeholder + '\t\t' + str(matrix[1, 1])
        return text


    def _print_sentiment_conf_matrixes(self, matrixes):
        text = ''
        for asp in matrixes:
            matrix = matrixes[asp]
            matrix_text = self._print_sentiment_conf_matrix(matrix)
            text += asp + ':\n'
            text += matrix_text + '\n\n'

        return text


    def _print_sentiment_conf_matrix(self, matrix):
        text = ''
        for row in matrix:
            for col in row:
                placeholder = ''
                if col < 10:
                    placeholder = '  '
                elif col < 100:
                    placeholder = ' '

                text += str(col) + placeholder + '\t'
            text += '\n'
        return text



    def _translate_aspects(self, aspects):
        zeros = np.zeros(len(self.aspect_index))
        for aspect in aspects:
            index = self.aspect_index[aspect]
            zeros[index] = 1

        return zeros





