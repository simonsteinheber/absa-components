from Core.DataLoading.DataProvider import DataProvider
from Core.IO.save import save_string_to_txt

class TextBuilder:

    def __init__(self, config):
        self.sources = config['sources']
        self.save_path = config['save_path']

    def build_text_file(self):
        texts = []
        for source in self.sources:
            path = source['data_path']
            format = source['data_format']
            dp = DataProvider(path, format)

            data = dp.get_data()
            texts += data.getText()

        result = ''
        for text in texts:
            result += text + '\n'

        #TODO: put to Core.IO.save
        if not result == '':
            save_string_to_txt(result, self.save_path)
