from DataPreparation.Extractor.GoldLabelExtractor import GoldLabelExtractor
from Core.IO.save import save_to_json

class AspectExtractor:

    def __init__(self, config, save_path):
        self.method = config['method']
        self.parameters = config['parameters']
        self.save_path = save_path

    def extract_aspects(self):
        if self.method == 'gold_labels':
            extractor = GoldLabelExtractor(**self.parameters)
        else:
            raise ValueError('method must be one of the following: \'gold_labels\'. Given value is: ' + self.method)

        aspect_index = extractor.extract()
        save_to_json(aspect_index, self.save_path)