import importlib

from Core.IO.load import load_word_vector_builder_register


class WordVectorBuilder:

    def __init__(self, corpus_path, save_path, method):
        self.corpus_path = corpus_path
        self.save_path = save_path
        self.method = self._get_method(method)

    def build_word_vecs(self):
        self.method(self.corpus_path, self.save_path)

    def _get_method(self, method):
        index = load_word_vector_builder_register()
        try:
            method = index[method]
        except:
            raise ValueError('method must be one of the following: ' + str(list(index.keys())) + '. Given value is: ' + method)

        module = importlib.import_module(method['module'])
        return getattr(module, method['name'])