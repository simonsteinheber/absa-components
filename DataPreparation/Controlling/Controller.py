import os

from DataPreparation.TextBuilder import TextBuilder
from DataPreparation.WordIndexBuilder import WordIndexBuilder
from DataPreparation.AspectExtractor import AspectExtractor
from DataPreparation.WordVectorBuilder import WordVectorBuilder
from DataPreparation.CharIndexBuilder import CharIndexBuilder
from Core.IO.save import check_path_existence

class Controller:

    def __init__(self, config, root):
        self.root = root
        self.corpus_building = config['corpus_building']
        self.aspect_extraction = config['aspect_extraction']
        self.word_vector_building = config['word_vector_building']
        self.language = config['language']
        self.word_index_building = config['word_index_building']
        self.char_index_building = config['char_index_building']

        self.run()

    def run(self):
        print('Build corpus')
        text_builder = TextBuilder(self.corpus_building)
        text_builder.build_text_file()

        print('Build word-vectors')
        word_vec_methods = self.word_vector_building['methods']
        for method in word_vec_methods:
            check_path_existence(self.root + '/Resources/Embedding/' + self.language)
            name = method['name']
            save_name = method['save_name']
            if name == 'fasttext':
                self._set_fasttext_name(save_name + '.bin')
            word_vector_builder = WordVectorBuilder(self.corpus_building['save_path'], self.root + '/Resources/Embedding/' + self.language + '/' + save_name, name)
            word_vector_builder.build_word_vecs()


        if self.word_index_building['build_word_index']:
            print('Build word-index')
            save_path = self.root + '/Resources/Indexes/' + self.language + '/' + self.word_index_building['index_name'] + '.json'
            fasttext_name = self._get_fasttext_name()
            try:
                word_index_builder = WordIndexBuilder(save_path, self.corpus_building['save_path'], self.root + '/Resources/Embedding/' + self.language + '/' + fasttext_name)
            except:
                word_index_builder = WordIndexBuilder(save_path, self.corpus_building['save_path'])
            word_index_builder.build_word_index()

        print('Build aspect-index')
        aspect_extractor = AspectExtractor(self.aspect_extraction, self.root + '/Resources/Indexes/' + self.language + '/' + self.aspect_extraction['index_name'] + '.json')
        aspect_extractor.extract_aspects()

        if self.char_index_building['build_char_index']:
            print('Build char-index')
            char_index_builder = CharIndexBuilder(self.char_index_building['allowed_chars'], self.root + '/Resources/Indexes/' + self.language + '/' + self.char_index_building['index_name'] + '.json')
            char_index_builder.build_char_index()


    def _set_fasttext_name(self, name):
        self.fasttext_name = name

    def _get_fasttext_name(self):
        try:
            return self.fasttext_name
        except:
            return None

