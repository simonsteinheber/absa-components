import os

from Core.IO.load import load_data_preparation_config
from DataPreparation.Controlling.Controller import Controller

def run():
    root = os.path.abspath('./../../')
    path = root + '/Resources/Config/'
    config = load_data_preparation_config(path)

    preparations = config['preparations']
    for preparation in preparations:
        print('Prepare data for ' + preparation['language'])
        Controller(preparation, root)
        print('Data is prepared')

run()