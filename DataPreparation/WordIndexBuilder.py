import numpy as np
import fasttext
import os
import re
from keras.preprocessing.text import text_to_word_sequence

from Core.IO.load import load_str_from_txt
from Core.IO.save import save_to_json

class WordIndexBuilder:

    def __init__(self, save_path, corpus_path, fasttext_path=None):
        self.corpus_path = corpus_path
        self.fasttext_path = fasttext_path
        self.save_path = save_path

        if fasttext_path:
            if not os.path.exists(fasttext_path):
                raise FileNotFoundError('The given path does not exist. Path: ' + fasttext_path)

    def build_word_index(self):
        texts = load_str_from_txt(self.corpus_path)
        words = text_to_word_sequence(texts, lower=False)

        if not self.fasttext_path == None:
            model = fasttext.load_model(self.fasttext_path)
            fast_words = list(model.words)
            words += fast_words


        filtered_words = self._filter_words(words)
        words += filtered_words
        words = np.unique(words)

        index = 0
        word_index = {}
        for word in words:
            word_index[word] = index
            index += 1

        word_index['MISC'] = index
        save_to_json(word_index, self.save_path)

    def _filter_words(self, words):
        result = []
        for word in words:
            filtered = re.sub('\ |\?|\.|\!|\/|\;|\:|\"|\'|\#|\@|\,|\+|\-|\*|\)|\(|\[|\]|1|2|3|4|5|6|7|8|9', '', word)
            if not filtered == '':
                result.append(filtered)

        return result
