import xml.etree.ElementTree as ET

from Core.IO.save import save_string_to_txt

def extract_texts(xml_path, save_path):
    tree = ET.parse(xml_path)

    text = ''

    root = tree.getroot()
    for product in root:
        for review in product:
            for entity in review:
                if entity.tag == 'text':
                    text += entity.text + '\n'

    save_string_to_txt(text, save_path)



extract_texts('/Users/simons/Projekte/ABSA-Publikation/Daten/Unsupervised/restaurant/restaurants.xml',
              '/Users/simons/Projekte/ABSA-Publikation/Daten/Unsupervised/restaurant/restaurants.txt')