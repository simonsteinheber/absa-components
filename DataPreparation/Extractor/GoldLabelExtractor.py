import operator

from Core.DataLoading.DataProvider import DataProvider


class GoldLabelExtractor:

    def __init__(self, data_path, data_format):
        self.dp = DataProvider(data_path, data_format)

    def extract(self):
        data = self.dp.get_data()
        aspect_sentiments = data.getAspectSentiments()

        aspects = []
        for review in aspect_sentiments:
            for asp, sentiment in review:
                if not asp in aspects:
                    aspects.append(asp)

        aspect_dict = {}
        for i, asp in enumerate(aspects):
            aspect_dict[asp] = i

        print(str(len(aspect_dict)) + " aspects have been extracted.")
        return aspect_dict
        '''
        aspects_dict = {}
        for review in aspect_sentiments:
            for asp, asp_sentiment in review:
                if not asp in aspects_dict:
                    aspects_dict[asp] = {'Number': 1,
                                         'Sentiment': {asp_sentiment: 1}}
                else:
                    aspects_dict[asp]['Number'] += 1
                    if not asp_sentiment in aspects_dict[asp]['Sentiment']:
                        aspects_dict[asp]['Sentiment'][asp_sentiment] = 1
                    else:
                        aspects_dict[asp]['Sentiment'][asp_sentiment] += 1

        main_dict = {}

        for asp in aspects_dict:
            main = asp.split('#')[0]
            if not main in main_dict:
                main_dict[main] = aspects_dict[asp]['Number']
            else:
                main_dict[main] += aspects_dict[asp]['Number']

        to_sort = {}
        for main in main_dict:
            to_sort[main] = main_dict[main]

        sorted_x = sorted(to_sort.items(), key=operator.itemgetter(1))

        indexed_aspects = {}
        for i, (main, num) in enumerate(sorted_x):
            indexed_aspects[main] = i

        
        return indexed_aspects
        '''


