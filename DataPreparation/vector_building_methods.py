import fasttext
from gensim.models import Word2Vec
import numpy as np
from keras.preprocessing.text import text_to_word_sequence

from Core.IO.save import save_string_to_txt

def build_fasttext_wordvecs(text_path, save_path):
    model = fasttext.skipgram(text_path, save_path, dim=200)
    print('Computed fasttext-word-vectors for ' + str(len(model.words)) + ' words.')


def build_w2vec_vecs(text_path, save_path):
    with open(text_path, 'r') as all:
        texts = all.read()

    texts = texts.split('\n')
    #texts = [[word for word in text.split(' ')] for text in texts ]
    texts = [text_to_word_sequence(text, lower=False) for text in texts]
    model = Word2Vec(texts, size=200)
    vecs = model.wv

    words = list(vecs.vocab.keys())
    not_found = []
    word_vecs = {}
    for word in words:
        try:
            word_vecs[word] = vecs[word]
        except:
            not_found.append(word)


    output = str(len(word_vecs)) + ' 200\n'
    for word in word_vecs:
        words = word.split(' ')

        output += words[0] + ' '
        vec = word_vecs[word]
        for val in vec:
            if type(val) == np.float32 or type(val) == np.float or type(val) == float:
                output += str(val) + ' '
            else:
                print(type(val))
        output += '\n'

    save_string_to_txt(output, save_path)

    '''
    with open(save_path, 'w') as text_file:
        print(output, file=text_file)
    '''

    print('Computed word2vec-word-vectors for ' + str(len(word_vecs)) + ' words.')