from Core.IO.save import save_to_json

class CharIndexBuilder:

    def __init__(self, allowed_chars, save_path):
        self.allowed_chars = allowed_chars
        self.save_path = save_path


    def build_char_index(self):
        index = {}
        counter = 0

        for char in self.allowed_chars:
            index[char] = counter
            counter += 1

        index['MISC'] = counter
        save_to_json(index, self.save_path)
