from Training.ModelBuilder.Components.Embedding.WordVectorLoading import WordVectorLoader
from Training.ModelBuilder.Components.Embedding.Embedder import *
from Training.ModelBuilder.Components.Encoder import build_cnn, build_lstm_bidirectional
from Training.ModelBuilder.Components.Output import build_output

class Builder:

    def __init__(self, builder_conf, general_conf):
        self.config = builder_conf
        self.general_conf = general_conf

    def build(self):
        embedder_conf = self.config['embedding']
        encoder_conf = self.config['encoder']
        output_conf = self.config['output']

        em_in, em_out, em_dim = self._build_embedding(embedder_conf)
        en_in, en_out = self._build_encoder(encoder_conf, em_out)

        output_conf['input'] = en_out
        outputs = build_output(**output_conf)

        return em_in, outputs

    def _build_embedding(self, config):
        word_index_path = self.general_conf['word_index_path']
        char_index_path = self.general_conf['char_index_path']
        root = self.general_conf['root']
        methods = config['methods']

        inputs = []
        outputs = []
        em_dim = 0
        for method in methods:
            word_vector_loader = WordVectorLoader(word_index_path, char_index_path, method, root)
            vecs = word_vector_loader.load_vecs()

            input, output, dim = self._embedder(vecs, method)
            inputs.append(input)
            outputs.append(output)
            em_dim += dim

        if len(outputs) > 1:
            output = concatenate(outputs)
        else:
            output = outputs[0]

        return inputs, output, em_dim


    def _embedder(self, vecs, method):
        if 'fasttext' in method or 'word2vec' in method or 'char_one_hot' in method:
            return build_basic_embedding(vecs)
        elif method == 'char_lstm':
            return build_lstm_character_embedding(vecs)
        elif method == 'char_cnn':
            return build_cnn_character_embedding(vecs)
        else:
            raise ValueError('method must be one of the following: \'fasttext\', \'word2vec\',\'char_one_hot\',\'char_lstm\',\char_cnn\'. The given value is: ' + method)

    def _build_encoder(self, config, input):
        method = config['method']
        parameter = config['parameters']
        parameter['input'] = input

        if method == 'cnn':
            return build_cnn(**parameter)
        elif method == 'lstm':
            return build_lstm_bidirectional(**parameter)
        else:
            raise ValueError('method must be one of the following: \'cnn\'. Given value is: ' + method)


