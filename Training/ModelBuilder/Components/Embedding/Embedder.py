from keras.layers import Embedding, Input, TimeDistributed, Conv1D, MaxPooling1D, Flatten, Reshape, Dense, LSTM, Concatenate

#TODO: Put to general config file
NUM_WORDS = 200
NUM_CHARS = 30

def build_cnn_character_embedding(char_matrix, num_aspects=None):
    em_dim = char_matrix.shape[1]
    if num_aspects == None:
        input = Input(shape=(NUM_WORDS,NUM_CHARS))
    else:
        input = Input(shape=(num_aspects,NUM_CHARS))

    embedding = TimeDistributed(Embedding(input_dim=char_matrix.shape[0], output_dim=em_dim, weights=[char_matrix],
                                          trainable=True))(input)
    computed = TimeDistributed(Conv1D(filters=128, kernel_size=4, padding='same'))(embedding)
    reshape = Reshape((int(computed.shape[1]), int(computed.shape[3]), int(computed.shape[2])))(computed)
    filtered = TimeDistributed(MaxPooling1D(32))(reshape)
    reshape = Reshape((int(filtered.shape[1]), int(filtered.shape[3]), int(filtered.shape[2])))(filtered)
    flatten = TimeDistributed(Flatten())(reshape)

    fully = TimeDistributed(Dense(1000))(flatten)
    output = TimeDistributed(Dense(em_dim))(fully)

    return input, output, em_dim


def build_lstm_character_embedding(char_matrix, num_aspects=None):
    em_dim = char_matrix.shape[1]
    if num_aspects == None:
        input = Input(shape=(NUM_WORDS,NUM_CHARS))
    else:
        input = Input(shape=(num_aspects,NUM_CHARS))

    embedding = TimeDistributed(Embedding(input_dim=char_matrix.shape[0], output_dim=em_dim, weights=[char_matrix],
                  trainable=True))(input)
    computed = TimeDistributed(LSTM(units=em_dim))(embedding)

    return input, computed, em_dim


def build_basic_embedding(word_vecs, trainable=False, num_aspects=None):
    em_dim = word_vecs.shape[1]
    if num_aspects == None:
        input = Input(shape=(NUM_WORDS,))
    else:
        input = Input(shape=(num_aspects,))
    embedded = Embedding(input_dim=word_vecs.shape[0], output_dim=em_dim, weights=[word_vecs], trainable=trainable)(input)

    return input, embedded, em_dim


def concatenate(outputs):
    return Concatenate(axis=2)(outputs)