import json
import fasttext
import numpy as np

from Core.IO.load import load_embedding_register

#TODO: Put to general config file
DIM = 200

class WordVectorLoader:

    def __init__(self, word_index_path, char_index_path, embedding_method, root):
        self.word_index = self._load_index(word_index_path)
        self.char_index = self._load_index(char_index_path)
        self.embedding_method = embedding_method

        if not 'char' in embedding_method:
            embedding_register = load_embedding_register()
            try:
                self.embedding_path = root + '/Resources/Embedding/' + embedding_register[embedding_method]
            except:
                raise ValueError('You defined the following keys in .../Resources/Register/embedding_register.json: ' + str(list(embedding_register.keys())) +
                                 '.\nBut you use \'' + embedding_method + '\' as embedding-method')


    def load_vecs(self):
        if 'fasttext' in self.embedding_method:
            loader = self.load_word_vecs_fasttext
        elif 'word2vec' in self.embedding_method:
            loader = self.load_word_vec_word2vec
        elif self.embedding_method == 'char_one_hot':
            loader = self.build_char_one_hot_word_vecs
        elif 'char' in self.embedding_method:
            return self._char_index_to_matrix()
        else:
            raise ValueError('embedding_method has to contain one of the following: \'fasttext\', \'word2vec\' or \'char\'. Also \'char_one_hot\' can be used. But the given value is: ' + self.embedding_method)

        word_vectors = loader()
        return self._get_vector_matrix(word_vectors)

    def load_word_vecs_fasttext(self):
        return fasttext.load_model(self.embedding_path)

    def load_word_vec_word2vec(self):
        embeddings_index = {}

        f = open(self.embedding_path)

        for line in f:
            values = line.split()
            if len(values) < DIM + 1:
                continue

            word = values[0]

            i = 1
            nofloat = True
            while nofloat:
                try:
                    value = np.asarray(values[i:], dtype='float32')
                    nofloat = False
                except:
                    i += 1
                    if len(values[i:]) < DIM:
                        nofloat = False
                        value = np.zeros((DIM,))

            if len(value) == DIM:
                embeddings_index[word] = value
        f.close()

        return embeddings_index

    def build_char_one_hot_word_vecs(self):
        result = {}
        alpha = 0.49

        for word in self.word_index:
            chars = []
            for char in list(word):
                zero = np.zeros(len(self.char_index) + 1)
                if char in self.char_index:
                    zero[self.char_index[char]] = 1
                else:
                    zero[self.char_index['MISC']] = 1
                chars.append(zero)

            word_em_front = np.zeros(len(self.char_index) + 1)
            word_em_back = np.zeros(len(self.char_index) + 1)
            for i, char in enumerate(chars):
                word_em_front += alpha**i * char

            chars.reverse()
            for i, char in enumerate(chars):
                word_em_back += alpha**i * char

            word_vec = np.concatenate([word_em_back, word_em_front])
            result[word] = word_vec

        return result

    def _char_index_to_matrix(self):
        char_index = self.char_index
        embedding_matrix = np.random.uniform(-0.5, .5, (len(char_index) + 1, DIM))

        print('Loaded %s char vectors.' % len(embedding_matrix))
        return embedding_matrix


    def _get_vector_matrix(self, word_vectors):
        embedding_matrix = self._zero_matrix()
        count = 0
        not_found = []
        for word, i in self.word_index.items():
            embedding_vector = None
            try:
                embedding_vector = word_vectors[word]
            except:
                not_found.append(word)

            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                count += 1
                embedding_matrix[i] = embedding_vector[:DIM]
                if i == 0:
                    print()

        print(str(round((count * 100 / len(embedding_matrix)),
                        2)) + '% of words have a representation in the pretrained embeddings')
        return embedding_matrix

    def _get_embedding_path(self, method):
        if method == 'fasttext_ger':
            return '/german/fasttext.bin'
        elif method == 'fasttext_eng':
            return '/english/fasttext.bin'
        elif method == 'word2vec_ger':
            return 'german/word2vec'
        elif method == 'word2vec_eng':
            return 'english/word2vec'
        else:
            raise ValueError('method must be one of the following: \'fasttext_ger\', \'fasttext_eng\', \'word2vec_ger\', \'word2vec_eng\'. Given value is: ' + method)

    def _zero_matrix(self):
        embedding_matrix = np.zeros((len(self.word_index) + 1, DIM))

        print('Loaded %s word vectors.' % len(embedding_matrix))
        return embedding_matrix

    def _load_index(self, path):
        with open(path, 'r') as wi:
            return json.load(wi)
