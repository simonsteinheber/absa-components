from keras.layers import Dense

from Core.NeuralNetwork.CustomLayers.FunctionExecutor import FunctionExecutor
from Core.NeuralNetwork.CustomActivations.CustomActivations import abs_activation


def build_output(input, output_dim, activation):
    num_outputs = 1
    if type(output_dim) == tuple:
        num_outputs = output_dim[0]
        output_dim = output_dim[1]

    outputs = []
    for i in range(num_outputs):
        if activation == 'sigmoid_3':
            output = Dense(units=output_dim, activation='sigmoid')(input)
            output = FunctionExecutor(abs_activation.__name__)(output)
        else:
            output = Dense(units=output_dim, activation=activation)(input)
        outputs.append(output)

    return outputs
