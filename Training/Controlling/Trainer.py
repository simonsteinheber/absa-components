import os
import sys
import traceback
import json

from Training.Controlling.Controller import Controller



def run():
    root = os.path.abspath('./../../')
    path = root + '/Resources/Config/'
    config = load_train_config(path)


    setups = config['setups']

    for setup in setups:
        '''
        try:
            print('Run setup')
            Controller(setup, root)
            print('Setup successful')
        except Exception as inst:
            print(inst)
            continue
        '''
        print('Run setup')
        Controller(setup, root)
        print('Setup successful')


def load_train_config(path):
    with open(path + '/train_config.json', 'r') as f:
        config = json.load(f)

    return config


run()