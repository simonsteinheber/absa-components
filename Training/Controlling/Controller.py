import numpy as np
import os
import json

from Core.Controlling.AbstractController import AbstractController
from Training.ModelBuilder.Builder import Builder
from Core.NeuralNetwork.ModelExtension import ModelExtension
from Core.DataLoading.DataProvider import DataProvider
from Prediction.ModelLoader.Loader import load_model_H5

class Controller(AbstractController):

    def __init__(self, config, root):
        '''
        Controlls the training process
        :param config: dict, representation of the setup-config. See Resources/Config/train_config.json or README for further information about structure
        :param root: string, path to the project-root
        '''

        super(Controller, self).__init__(config, root)
        self.model = config['model']
        self.training = config['training']

        self._save_config(config, root + '/Resources/Model/' + self.general['name'] + '/train_config.json')
        self.cos = self.build_cos()
        self.run()

    def run(self, model=None):
        #DATA-LOADING
        data = self.load_data()

        #DATA-CONVERSION
        samples, labels = self.convert_data(data)

        #MODEL-BUILDING
        if model == None:
            output_dim = (len(labels), labels[0].shape[1])
            self.model['output']['output_dim'] = output_dim
            model_in, model_out = self._build_model()
            self.training['inputs'] = model_in
            self.training['outputs'] = model_out
            model = ModelExtension(**self.training)
            model.build_model()

        #TRAINING
        model.train(samples,labels)

        #PSEUDO-LABELING
        if self.general['pseudo_labeling']:
            self._pseudo_labeling(model)

        #SAVING
        model.save_hist(self.root + '/Resources/Model/' + self.general['name'] + '/training_values.json')
        model.save_to_HDF5(self.root + '/Resources/Model/' + self.general['name'] + '/model.h5')

    def _pseudo_labeling(self, model):
        try:
            dp = DataProvider(self.data_loading['pseudo_path'], self.data_loading['pseudo_format'])
        except:
            raise ValueError ('If you want to use pseudo-labeling, you have to give the path and format of the unlabeled data in section \'data_loading\'. \n'
                              'The keys are: \'pseudo_path\', \'pseudo_format\'.')
        data = dp.get_data()
        samples, labels = self.convert_data(data, False)
        labels = model.test(samples)
        labels = self._norm_labels(labels)
        model.train(samples, labels)

        self.general['pseudo_labeling'] = False
        self.run(model)


    def _norm_labels(self, labels):
        result = []
        for label in labels:
            if type(label[0]) == list or type(label[0]) == np.ndarray:
                result.append(self._norm_labels(label))
            else:
                zeros = np.zeros(len(label))
                index = np.argmax(label)
                zeros[index] = 1
                result.append(zeros)

        return list(np.array(result))


    def _build_model(self):
        builder = Builder(self.model, self.general)
        return builder.build()

    def _save_config(self, config, path):
        path_without_file = path.split('/')[:-1]
        temp = ''
        for elem in path_without_file:
            temp += elem + '/'
        path_without_file = temp
        if not os.path.exists(path_without_file):
            os.makedirs(path_without_file)

        with open(path, 'w') as f:
            json.dump(config, f)
