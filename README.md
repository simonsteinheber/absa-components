# Requirements
To run this project you will need `pyhton 3.6` and the following packages:
 - keras
 - tensorflow (for GPU build from source: https://www.tensorflow.org/install/install_sources)
 - h5py
 - gensim
 - sklearn
 - imbalanced-learn
 - fasttext

All can be installed using `pip install`

# Structure
This Project  is structured in five subprojects: Core, DataPreparation, Training, Prediction, Validation.
The Core-project gathers all common functionalities. The other projects symbolize one step in the process of Aspect-Based Sentiment-Analysis from start to end. Each step has a config-file which can be found in **/Resources/Config**. 
They have to be adjusted to your own interests. How will be described in the following. 

## Core
This projects contains methods for data-loading, data-conversion, IO and custom `keras` implementations. 

### Data-Loading
For each desired data-type you have to implement a translator-class that at least implements a function `translate()` and takes the path to the file as constructor-variable.
See **/Core/DataLoading/Translator/XMLTranslatorSemEval.py** as example of a translator-class.
After you implemented the class you have to register it. Therefor add it to **/Resources/Register/translator_register.json** which looks like this:
```json
{
  "xml_semeval": {
    "module": "Core.DataLoading.Translator.XMLTranslatorSemEval",
    "class": "XMLTranslatorSemEval"
  },
  "xml_germeval": {
    "module": "Core.DataLoading.Translator.XMLTranslatorGermEval",
    "class": "XMLTranslatorGermEval"
  },
  "txt": {
    "module": "Core.DataLoading.Translator.TXTTranslator",
    "class": "TXTTranslator"
  }
}
```
The key (e.g. "txt") can be used to refer to this data-type in other config-files. 

### Data-Conversion
This module defines conversion-methods, that convert the intern data to the desired representations or the other way round. To add new conversion-methods you have to adjust the `convert()`-method of the specific Converter-class.
E.g. to implement a method that converts an output-text to a new representation, you have to adjust the `convert()`-method in **/Core/DataConversion/TextToRepr/OutputConverter.py**


## DataPreparation
To run this subproject run `python DataPreparator.py` in **/DataPreparation/Controlling**
This step does all the stuff needed before you can do the actual training. 
The config-file looks like the following. The parts will be explained in the subsections
```json
{
  "preparations": [
    {
      "language": "german",
      "word_index_building": {...
      },
      "corpus_building": {...
      },
      "aspect_extraction": {...
      },
      "word_vector_building": {...
      },
      "char_index_building": {...
      }
    }
  ]

}
```

### Corpus
You can specify all the text-sources you collected and then a corpus which contains all of these sources will be built.

```json
"corpus_building": {
    "save_path": ".../dev/workspaces/ABSA-Components/Resources/Corpus/german.txt",
    "sources": [
        {
            "data_path": ".../train_v1.4.xml",
            "data_format": "xml_germeval"
        },
        {
            "data_path": ".../unsupervised.txt",
            "data_format": "txt"
        },
      ...
    ]
}
```
As "data\_format" all keys defined in translator_register.json can be used

### Word-Vecs
You can use the text-corpus that, has been built in the previous step to train word-vecs. If you don't want to build a corpus but you already have one you can train word-vecs on just give a path to this file in the corpus-build section.

```json
"word_vector_building": {
    "methods": [
      {
        "name": "fasttext",
        "save_name": "fasttext"
      },
      {
        "name": "word2vec",
        "save_name": "word2vec.txt"
      }
    ]
}
```
As name only 'fasttext' and 'word2vec' can be used. The trained vectors will be saved as **/Resources/Embedding/\<language\>/\<save_name\>**

### Word-Index

```json
"word_index_building": {
    "build_word_index": true,
    "index_name": "word_index.json"
}
```
The index will be save in /Resources/Indexes/\<language\>/\<index_name\>

### Aspect-Index
This part extracts aspects from a defined file and saves the found aspects to **/Resources/Indexes/\<language\>/\<index_name\>.json**

```json
"aspect_extraction": {
    "index_name": "aspect_index_sem_eval_restaurants",
    "method": "gold_labels",
    "parameters": {
      "data_path": ".../train_v1.4.xml",
      "data_format": "xml_germeval"
    }
}
```

The method "gold\_labels" is the only implemented method to extract aspects. Therefor you need annotated data and the aspect-labels will be used as the aspects. 
To implement an other aspect-extraction-method you have to adjust the `extract_aspects()`-method in **/DataPreparation/AspectExtrator.py**

### Char-Index
This part builds a char-index which is necessary to use char-embeddings. The index will be stored in **/Resources/Indexes/\<language\>/\<index_name\>.json**

```json
"char_index_building": {
    "index_name": "char_index",
    "build_char_index": true,
    "allowed_chars": "äüöabcdefghijklmnopqrstuvwxyzÄÖÜABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?=)(/&%$§\"!'#+*-.,;:ß´`><"
}
```


## Training
To run this subproject run `python Trainer.py` in **/Training/Controlling**
This step does the actual training.
The config-file looks like the following. The parts will be explained in the subsections

```json
{
  "setups":[
    {
      "general": {...
      },
      "data_loading": {...
      },
      "conversion": {...
      },
      "model": {...
      },
      "training": {...
      }
    },
    ...
  ]
}
```

### General

```json
"general": {
    "aspect_index_path": ".../ABSA-Components/Resources/Indexes/german/aspect_index_germ_eval.json",
    "word_index_path": ".../ABSA-Components/Resources/Indexes/german/word_index.json",
    "char_index_path": ".../ABSA-Components/Resources/Indexes/german/char_index.json",
    "name": "my_new_trained_model",
    "pseudo_labeling": false
}
```
After running the DataPreparation you should have the desired index-files. You have to specify these explictly so the system works on your computer.
The trained model and all other belonging files will be stored in **/Resources/Model/<"name">/**
If you want to use pseudo-labeling, you have to set "pseudo\_labeling" to `true`

### Data-Loading
The data_path describes where to find the train-data. The pseudo-path describes where to find the data that should be used for pseudo-labeling.

```json
"data_loading": {
    "data_path": ".../train_v1.4.xml",
    "data_format": "xml_germeval",
    "pseudo_path": ".../restaurants.txt",
    "pseudo_format": "txt"
}
```


### Conversion
```json
"conversion": {
    "input_conversions": ["word_indexes", "char_indexes_per_word"],
    "output_conversions": ["sent_per_asp"]
}
```

The input\_conversions have to match the methods defined in the embedding-part. "word\_indexes" is used if you use a word-embedding like fasttext. "char\_indexes\_per\_word" is used if you want to use a char-level-embedding like "char\_lstm".
As output\_conversion only "sent\_per\_asp" is defined at the moment. To define other output-conversions refer to section Core.DataConversion

### Model Definition

```json
"model": {
    "embedding": {
      "methods": ["fasttext_ger", "char_lstm"]
    },
    "encoder": {
      "method": "lstm",
      "parameters": {
        "dropout": 0.5,
        "embedding_dim": 200
      }
    },
    "output": {
      "activation": "softmax"
    }
}
```
The embedding-methods have to match the input\_conversions defined in the conversion. You can use char-level-embeddings by "char\_lstm", "char\_cnn",  "char\_one\_hot".
Additionally you can register pretrained word-vectors in **/Resources/Register/embedding_register.json**, that looks like this:

```json
{
  "fasttext_ger": "/german/fasttext.bin",
  "word2vec_ger": "/german/word2vec.txt",
  ...
}
```
The specified path will be concatenated with **.../Resources/Embedding/** so you have to store your word-vectors at least in this folder. 
But if you use the word-vector-training of the DataPreparation-Project they will be stored there.

### Training

```json
"training": {
    "batch_size": 16,
    "epochs": 20,
    "optimizer": "sgd",
    "penalization": "None"
}
```

optimizer can be one of: "sgd", "adam", "rms_prop"
penalization can be one of: "sqrt", "log", "sqrt\_log", "quad", "quad\_log\_avg", "basic", "None"


## Prediction

```json
{"setups":
  [
    {
      "data_path": ".../ABSA-SemEval2016/Restaurant/test_gold.xml",
      "data_format": "xml_semeval",
      "name": "my_new_trained_model"
    },
    ...
  ]
{
```

name defines which trained model should be used for the prediction. Therefor you have to train a model with the name before. 
The data\_path defines where the test-data can be found.
At the moment only xml-data can be stored (There are also only xml-translators at the moment). If you already defined a new Translator like described in Core.DataLoading and your test-data is in the same format you can define an new Saver.
To see how a saver-class should look, see e.g. **/Prediction/DataSaving/Saver/XMLSaverGermEval.py**.
If you implemented a new saver-class you have to register it in **/Resources/Register/saver_register.json**.

## Validation

```json
{
  "setups": [
    {
      "name": "semeval_restaurant_fasttext_char_lstm_batch_size_16_basic_pena",
      "gold_path": ".../ABSA-SemEval2016/Restaurant/test_gold.xml",
      "gold_format": "xml_semeval",
      "predicted_format": "xml_semeval",
      "aspect_index_path": ".../ABSA-Components/Resources/Indexes/english/aspect_index_sem_eval_restaurants.json"
    },
    ...
  ]
{
```

name defines which model to validate
gold_path defines the gold-labels the predictions should be compared with.
At the moment the prediction is saved at the model-folder as predicted.xml. So only the xml-format have to be specified to load the predictions. This should be adjusted to be more flexible.
Therefor see **/Validation/Validator.py** (line 15).
Also the aspect\_index\_path has to be defined. This should be the same as in train\_config.json


# GPU-Computation
For training GPU-Servers of the LRZ have been used. In **/Resources/GPU-Setups/** are four bash-files which will set up one of these Servers. The paths used in local\_load1.bh and local\_load2.bh have to be adjusted for a proper use.
