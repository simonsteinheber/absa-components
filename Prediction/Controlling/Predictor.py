import os
import sys
import json

sys.path.insert(0, os.path.abspath('./../../'))
from Prediction.Controlling.Controller import Controller


def run():
    root = os.path.abspath('./../../')
    path = root + '/Resources/Config/'
    config = load_predict_config(path)


    setups = config['setups']

    for setup in setups:
        '''
        try:
            print('Run setup')
            Controller(setup, root)
            print('Successful for: ')
        except Exception as inst:
            print(inst)
            continue
        '''
        print('Run setup')
        Controller(setup, root)
        print('Successful for: ')


def load_predict_config(path):
    with open(path + '/predict_config.json', 'r') as f:
        config = json.load(f)

    return config


run()