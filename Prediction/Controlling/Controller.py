
import numpy as np

from Prediction.ModelLoader.Loader import load_model_H5
from Prediction.DataSaving.DataSaver import DataSaver

from Core.Controlling.AbstractController import AbstractController
from Core.DataConversion.ReprToText.OutputConverter import OutputConverter


class Controller(AbstractController):

    def __init__(self, config, root):
        super(Controller, self).__init__(config, root, True)

        self.batch_size = self._load_batch_size()
        self.cos = self.build_cos()

        self.run()

    def run(self):
        #DATA-LOADING
        data = self.load_data()

        #DATA-CONVERSION
        samples, labels = self.convert_data(data)

        #MODEL-LOADING
        model = load_model_H5(self.root + '/Resources/Model/' + self.general['name'] + '/model.h5', self.cos)
        model.set_batch_size(self.batch_size)

        #PREDICTION
        result = model.test(samples)

        #DATA-BACK-CONVERSION
        converted_result = self._convert_result(result, data.getAspectSentiments())

        #DATA-SAVING
        self._save_data(converted_result)

    def _convert_result(self, result, labels):
        conversions = self.conversion['output_conversions']
        aspect_index_path = self.general['aspect_index_path']
        class_index = self.build_class_index(labels)

        conversion = conversions[0]
        if conversion == 'sent_per_asp':
            result = np.array(result)
            result = list(np.swapaxes(result, 0, 1))

        output_converter = OutputConverter(result, conversion, aspect_index_path, class_index)
        converted_output = output_converter.convert()

        return converted_output

    def _save_data(self, result):
        xml_path = self.general['data_path']
        format = self.general['data_format']
        save_path = self.root + '/Resources/Model/' + self.general['name'] + '/predicted.xml'
        data_saver = DataSaver(result, xml_path, save_path, format)

        data_saver.save()

    def _load_batch_size(self):
        train_config = self.load_train_config(self.root + '/Resources/Model/' + self.general['name'])
        return train_config['training']['batch_size']
