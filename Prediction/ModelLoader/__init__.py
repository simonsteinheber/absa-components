import sys
import os

file_path = os.path.dirname(os.path.abspath(__file__))
splitted = file_path.split('/')

root_splitted = splitted[:-2]
root = ''
for dir in root_splitted:
    root += dir + '/'

sys.path.append(root)