from Core.NeuralNetwork.ModelExtension import ModelExtension
from keras.models import load_model

def load_model_H5(path, cos):
    print('Load Model from ' + path)
    with cos:
        loaded = load_model(path)

    return ModelExtension(loaded.input, loaded.output)