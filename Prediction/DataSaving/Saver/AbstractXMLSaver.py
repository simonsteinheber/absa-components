class AbstractXMLSaver:

    def __init__(self, result, path_to_xml, save_path):
        self.result = result
        self.path_to_xml = path_to_xml
        self.save_path = save_path

    def _save_xml_tree(self, tree):
        tree.write(self.save_path, encoding='UTF-8')
        print('Saved results to ' + self.save_path)