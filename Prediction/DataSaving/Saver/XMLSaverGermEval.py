from xml.etree import ElementTree as ET

from Prediction.DataSaving.Saver.AbstractXMLSaver import AbstractXMLSaver

class XMLSaverGermEval(AbstractXMLSaver):

    def __init__(self, result, path_to_xml, save_path):
        super(XMLSaverGermEval, self).__init__(result, path_to_xml, save_path)

    def save(self):
        '''
        Saves the given result to the specified path.
        '''
        # Can be extended to call different save-methods for different outputs
        self._handle_abs_result()

    def _handle_abs_result(self):
        tree = ET.parse(self.path_to_xml)
        tree = self._put_abs_result_to_xml(tree)

        self._save_xml_tree(tree)
        print('Process finished')

    def _put_abs_result_to_xml(self, tree):
        root = tree.getroot()
        index = 0
        for document in root:
            self._set_ab_sentiment_in_document(document, self.result[index])
            index += 1

        return tree

    def _set_ab_sentiment_in_document(self, document, ab_sentiments):
        ab_sentiments_dict = []
        for ab_sent in ab_sentiments:
            ab_sent_dict = {'category': ab_sent[0],
                            'polarity': ab_sent[1]}
            ab_sentiments_dict.append(ab_sent_dict)

        for entity in document:
            if (entity.tag == 'Opinions'):
                for opinion in list(entity):
                    entity.remove(opinion)
                for ab_sent in ab_sentiments_dict:
                    ET.SubElement(entity, 'Opinion', ab_sent)