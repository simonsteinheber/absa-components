from Prediction.DataSaving.Saver.XMLSaverGermEval import *
from Prediction.DataSaving.Saver.XMLSaverSemEval import *

class DataSaver:

    def __init__(self, result, path, save_path, format):
        self.result = result
        self.path = path
        self.save_path = save_path
        self.format = format

    def save(self):
        if self.format == 'xml_semeval':
            saver = XMLSaverSemEval(self.result, self.path, self.save_path)
        elif self.format == 'xml_germeval':
            saver = XMLSaverGermEval(self.result, self.path, self.save_path)
        else:
            raise ValueError('format must be one of the following: \'xml_semeval\', \'xml_germeval\'. Given value is: ' + self.format)

        saver.save()