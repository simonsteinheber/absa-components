from keras.layers import Layer
from Core.NeuralNetwork.CustomActivations.CustomActivations import get

class FunctionExecutor(Layer):

    def __init__(self, function, **kwargs):
        super(FunctionExecutor, self).__init__(**kwargs)
        self.function = function

    def build(self, input_shape):
        super(FunctionExecutor, self).build(input_shape)

    def get_output_shape_for(self, input_shape):
        return input_shape

    def compute_output_shape(self, input_shape):
        return input_shape

    def call(self, x, mask=None):
        return get(self.function)(x)

    def get_config(self):
        config = {'function': self.function}
        base_config = super(FunctionExecutor, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))