from keras.models import Model
import json
import os

from Core.NeuralNetwork.WeightProvider import WeightProvider

class ModelExtension(Model):

    def __init__(self, inputs=None, outputs=None, epochs=None, optimizer=None, batch_size=None, penalization=None, name='Model'):
        super(ModelExtension, self).__init__(inputs, outputs, name)
        self.num_outputs = 1
        self.batch_size = 64
        if type(outputs) == list:
            self.num_outputs = len(outputs)
        if not epochs == None:
            self.batch_size = batch_size
            self.penalization = penalization
            self.optimizer_str = optimizer
            self.epochs = epochs
            self.hist = []

    def build_model(self):
        self.compile(loss='categorical_crossentropy', optimizer=self.optimizer_str, metrics=['accuracy'])

    def train(self, train_x, train_y):
        weight_provider = WeightProvider(train_y, self.penalization)
        sample_weights = weight_provider.get_sample_weights()

        print('Training..')
        hist_tmp = self.fit(train_x, train_y, validation_split=0.1, epochs=self.epochs, shuffle=True,
                            sample_weight=sample_weights, batch_size=self.batch_size, verbose=1)
        self.hist.append(hist_tmp.history)

    def test(self, test_x):
        results = self.predict(test_x, batch_size=self.batch_size)
        return results

    def save_to_HDF5(self, path):
        path_without_file = path.split('/')[:-1]
        temp = ''
        for elem in path_without_file:
            temp += elem + '/'
        path_without_file = temp
        if not os.path.exists(path_without_file):
            os.makedirs(path_without_file)

        self.save(path)

    def save_hist(self, path):
        path_without_file = path.split('/')[:-1]
        temp = ''
        for elem in path_without_file:
            temp += elem + '/'
        path_without_file = temp
        if not os.path.exists(path_without_file):
            os.makedirs(path_without_file)

        with open(path, 'w') as f:
            json.dump(self.hist, f)

    def get_params(self):
        return (self.optimizer_str, self.epochs)

    def get_config(self):
        base_config = super().get_config()
        return base_config

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size

    def set_epochs(self, epochs):
        self.epochs = epochs