import numpy as np
from math import sqrt, log

class WeightProvider:

    def __init__(self, labels, penalization):
        '''
        Provides sample-weights for every given output-distribution
        Every output is weighted by the same function
        Todo: Adjust so that every output can be weighted by a explicit function
        :param labels: all outputs, they are used to compute the class-distribution
        :param penalization: Description of the penalization-method to be used
        '''

        self.labels = labels
        self.penalization = penalization


    def get_sample_weights(self):
        if self.penalization == 'None':
            return None

        weights = []
        for output in self.labels:
            weights.append(self._get_sample_weights_per_output(output))

        return weights

    def _get_sample_weights_per_output(self, labels):
        translated_labels = []
        for label in labels:
            translated_labels.append(self._translate(label))

        weights = self._get_class_weights(translated_labels)
        weights = self._apply_pena_function(weights)

        result = []
        for label in translated_labels:
            result.append(self._get_label_weight(weights, label))

        return np.array(result)

    def _get_label_weight(self, weights, label):
        for _class, weight in weights:
            if _class == label:
                return weight

    def _translate(self, label):
        return np.argmax(label) + 1

    def _get_class_weights(self, labels):
        classes = np.unique(labels, axis=0)

        if not type(labels) == np.ndarray:
            labels = np.array(labels)

        weights = [(_class, len(list(np.where(labels == _class))[0])) for _class in classes]
        max = np.max(weights)
        weights = self._relate_to_max(weights, max)

        return weights

    def _relate_to_max(self, weights, max):
        result = []
        for _class, num in weights:
            new_num = max / num
            result.append((_class, new_num))

        return result

    def _apply_pena_function(self, weights):
        if self.penalization == 'sqrt':
            weights = [(_class, sqrt(weight)) for _class, weight in weights]

        elif self.penalization == 'sqrt_log':
            new_weights = []
            for _class, weight in weights:
                if not weight == 1.0:
                    new_weights.append((_class, sqrt(weight) + log(weight)))
                else:
                    new_weights.append((_class, weight))
            weights = new_weights

        elif self.penalization == 'log':
            new_weights = []
            for _class, weight in weights:
                new_weights.append((_class, log(weight) + 1))
            weights = new_weights

        elif self.penalization == 'quad':
            new_weights = []
            for _class, weight in weights:
                if not weight == 1.0:
                    new_weights.append((_class, ((weight + 5) / 7) ** 2 + 1.5))
                else:
                    new_weights.append((_class, weight))
            weights = new_weights

        elif self.penalization == 'quad_log_avg':
            new_weights = []
            for _class, weight in weights:
                if not weight == 1.0:
                    new_weights.append((_class, ((((weight + 5) / 7) ** 2 + 1.5 + log(weight) + 1) / 2)))
                else:
                    new_weights.append((_class, weight))
            weights = new_weights

        elif self.penalization == 'basic':
            pass

        else:
            raise ValueError('penalization must be on of the following: \'sqrt\', \'log\', \'sqrt_log\', \'quad\', \'quad_log_avg\' \'basic\'. Given value: ' + self.penalization)

        return weights