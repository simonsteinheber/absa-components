from keras import backend as K
import tensorflow as tf

#TODO: Put to general config file
THRES = 0.25
NA = K.variable([100.0, 0.00000000001, 0.00000000001, 0.00000000001])
ZEROS = K.variable([0.00000000001, 0.00000000001, 0.00000000001, 0.00000000001])

def gated_multiply(x):
    asp_output = x[1]
    gate = x[2]
    asp_pred = K.squeeze(K.squeeze(tf.matmul(K.expand_dims(asp_output, 0), K.expand_dims(gate, 0), transpose_b=True), 0), 0)
    return tf.cond(K.less(asp_pred, THRES), lambda: tf.add(x[0], NA), lambda: x[0])

def abs_activation(x):
    return tf.multiply(x, 3)

def null_norm_multiply(x):
    abs_output = x[0]
    asp_output = x[1]
    gate = K.expand_dims(x[2],0)

    return tf.map_fn(gated_multiply, [abs_output, asp_output, gate], dtype=K.floatx())

    #return tf.map_fn(lambda x: tf.map_fn(gated_multiply, x, dtype=K.floatx()), [abs_outputs, asp_output], dtype=K.floatx())


def my_softmax(x):
    em_dim = x.shape[0]
    all = K.repeat_elements(K.expand_dims(K.sum(x),0), em_dim, 0)
    return tf.map_fn(lambda x: tf.divide(x[0],x[1]), (x,all), dtype=K.floatx())


def get(name):
    if name == abs_activation.__name__:
        return abs_activation
    if name == null_norm_multiply.__name__:
        return null_norm_multiply
    if name == my_softmax.__name__:
        return my_softmax