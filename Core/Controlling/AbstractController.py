import json
import numpy as np
from keras.utils import CustomObjectScope

from Core.DataLoading.DataProvider import DataProvider
from Core.NeuralNetwork.ModelExtension import ModelExtension
from Core.NeuralNetwork.CustomLayers.Attention import *
from Core.NeuralNetwork.CustomLayers.FunctionExecutor import *
from Core.DataConversion.TextToRepr.OutputConverter import OutputConverter
from Core.DataConversion.TextToRepr.InputConverter import InputConverter


class AbstractController:

    def __init__(self, config, root, is_predict_config=False):
        self.root = root

        if is_predict_config:
            train_config = self.load_train_config(self.root + '/Resources/Model/' + config['name'])
            self.data_loading = config
            self.conversion = train_config['conversion']
            self.general = train_config['general']
            self.general['root'] = root
            self.general['data_path'] = config['data_path']
            self.general['data_format'] = config['data_format']
        else:
            self.general = config['general']
            self.general['root'] = root
            self.data_loading = config['data_loading']
            self.conversion = config['conversion']

    def load_data(self):
        data_path = self.data_loading['data_path']
        data_format = self.data_loading['data_format']
        dp = DataProvider(data_path, data_format)

        return dp.get_data()

    def convert_data(self, data, labeled=True):
        input_conversions = self.conversion['input_conversions']
        output_conversions = self.conversion['output_conversions']
        word_index_path = self.general['word_index_path']
        char_index_path = self.general['char_index_path']
        aspect_index_path = self.general['aspect_index_path']

        converted_texts = []
        for conversion in input_conversions:
            converter = InputConverter(data.getText(), conversion, word_index_path, char_index_path)
            converted_texts.append(converter.convert())

        converted_labels = None
        if labeled:
            converted_labels = []
            for conversion in output_conversions:
                class_index = self.build_class_index(data.getAspectSentiments())
                converter = OutputConverter(data.getAspectSentiments(), conversion, word_index_path, aspect_index_path, class_index)
                converted_labels += converter.convert()

        return converted_texts, converted_labels

    def load_train_config(self, path):
        with open(path + '/train_config.json', 'r') as f:
            config = json.load(f)

        return config

    def build_class_index(self, labels):
        i = 0
        new_labeles = []
        while i < len(labels):
            label = labels[i]
            if not label == []:
                new_labeles.append(label)
            i += 1
        labels = new_labeles

        labels = np.concatenate(labels)
        labels = labels[:, 1]
        classes = np.unique(labels, axis=0)

        result = {'N/A': 0}
        for i, _class in enumerate(classes):
            result[_class] = i + 1

        return result

    def build_cos(self):
        return CustomObjectScope({'ModelExtension': ModelExtension,
                           'FunctionExecutor': FunctionExecutor,
                           'Attention': Attention})


