from keras.preprocessing.text import text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
import numpy as np

from Core.DataConversion.AbstractConverter import AbstractConverter

#TODO: Put to general config file
NUM_CHARS = 30
NUM_WORDS = 200

class InputConverter(AbstractConverter):

    def __init__(self, input, conversion, word_index_path, char_index_path):
        '''
        This class provides a convert-method, that converts the given input dependent on the defined conversion
        :param input: All texts as strings
        :param conversion: String that defines how the text should be converted
        :param word_index_path: Path to JSON-file that stores indexes for the known words
        :param char_index_path: Path to JSON-file that stores indexes for the known chars
        '''

        super(InputConverter, self).__init__(input, conversion)
        self.word_index = self._load_index(word_index_path)
        self.char_index = self._load_index(char_index_path)

    def convert(self):
        if self.conversion == 'word_indexes':
            converter = self._convert_text_to_word_indexes
        elif self.conversion == 'char_indexes':
            converter = self._convert_text_to_char_indexes
        elif self.conversion == 'char_indexes_per_word':
            converter = self._convert_text_to_char_indexes_per_word
        else:
            raise(ValueError('conversion must be on of the following: \'word_indexes\', \'char_indexes\' or \'char_indexes_per_word\'. Given value: ' + self.conversion))

        converted = []
        for text in self.instances:
            converted.append(converter(text))

        return pad_sequences(converted, NUM_WORDS)

    def _convert_text_to_word_indexes(self, text):
        words = self._get_word_sequence(text)
        word_indexes = []
        for word in words:
            try:
                word_indexes.append(self.word_index[word])
            except:
                word_indexes.append(self.word_index['MISC'])

        return word_indexes

    def _convert_text_to_char_indexes(self, text):
        index = self.char_index

        char_indexes = []
        for char in list(text):
            try:
                char_indexes.append(index[char])
            except:
                char_indexes.append(self.word_index['MISC'])

        return char_indexes


    def _convert_text_to_char_indexes_per_word(self, text):
        text_arr = []
        index = self.char_index

        words = self._get_word_sequence(text)
        for word in words:
            word_chars = []
            for char in list(word):
                try:
                    word_chars.append(index[char])
                except:
                    word_chars.append(0)

            text_arr.append(np.array(word_chars))
        return pad_sequences(text_arr, NUM_CHARS)


    def _get_word_sequence(self, text):
        return text_to_word_sequence(text, filters='\t\n', lower=False)







