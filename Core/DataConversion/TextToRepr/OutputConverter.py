import numpy as np


from Core.DataConversion.AbstractConverter import AbstractConverter

class OutputConverter(AbstractConverter):

    def __init__(self, output, conversion, word_index_path, aspect_index_path, class_index):
        '''
        This class provides a convert-method, that converts the given input dependent on the defined conversion
        :param input: All texts as strings
        :param conversion: String that defines how the text should be converted
        :param word_index_path: Path to JSON-file that stores indexes for the known words
        :param char_index_path: Path to JSON-file that stores indexes for the known chars
        :param class_index: Dict describing the possible classes (Sentiments)
        '''

        super(OutputConverter, self).__init__(output, conversion)
        self.word_index = self._load_index(word_index_path)
        self.asp_index = self._load_index(aspect_index_path)
        self.class_index = class_index


    def convert(self):
        if self.conversion == 'sent_per_asp':
            converter = self._convert_asp_sent_sent_per_asp
        elif self.conversion == 'aspect':
            converter = self._convert_aspects_to_array
        else:
            raise ValueError('conversion must be on of the following: \'sent_per_asp\', \'aspect\'. Given value: ' + self.conversion)

        converted = []
        for output in self.instances:
            converted.append(converter(output))

        if self.conversion == 'sent_per_asp':
            converted = np.array(list(converted))
            return list(np.swapaxes(converted, 0, 1))
        else:
            return np.array(converted)


    def _convert_asp_sent_sent_per_asp(self, asp_sents):
        result = [None] * self._get_num_aspects()

        for aspect, sentiment in asp_sents:
            zeros = np.zeros(len(self.class_index))
            index_asp = self.asp_index[aspect]
            index_sent = self.class_index[sentiment]
            zeros[index_sent] = 1
            result[index_asp] = zeros

        for i, aspect in enumerate(result):
            if not type(aspect) == np.ndarray:
                zeros = np.zeros(len(self.class_index))
                zeros[0] = 1
                result[i] = zeros

        return np.array(result)

    def _convert_aspects_to_array(self, aspects):
        result = []
        for aspect in aspects:
            zeros = np.zeros(self._get_num_aspects())
            index = self.asp_index[aspect]
            zeros[index] = 1
            result.append(zeros)

        return np.sum(result, axis=0)


    def _get_num_aspects(self):
        return len(self.asp_index)
