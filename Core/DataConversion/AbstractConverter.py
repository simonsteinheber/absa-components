import json

class AbstractConverter:

    def __init__(self, instances, conversion):
        self.instances = instances
        self.conversion = conversion


    def _load_index(self, path):
        with open(path, 'r') as wi:
            return json.load(wi)