import numpy as np

from Core.DataConversion.AbstractConverter import AbstractConverter

#TODO: Put to general config file
THRES = 0.25


class OutputConverter(AbstractConverter):

    def __init__(self, output, conversion, aspect_index_path, class_index):
        super(OutputConverter, self).__init__(output, conversion)

        #Change keys & values in aspect-index
        self.aspect_index = {v: k for k, v in self._load_index(aspect_index_path).items()}
        self.class_index = {v: k for k, v in class_index.items()}

    def convert(self):
        if self.conversion == 'aspect':
            converter = self.convert_pred_array_to_aspect_list
        elif self.conversion == 'sent_per_asp':
            converter = self.convert_sent_per_asp_pred_array_to_aspect_sentiment
        else:
            raise ValueError('conversion must be on of the following: \'aspect\', \'sent_per_asp\'. Given value: ' + self.conversion)

        converted = []
        for output in self.instances:
            converted.append(converter(output))

        return converted



    def convert_pred_array_to_aspect_list(self, arr):
        result = []
        for i, aspect in enumerate(arr):
            if aspect > THRES:
                result.append(self.aspect_index[i])

        return result

    def convert_sent_per_asp_pred_array_to_aspect_sentiment(self, arr):
        result = []
        for asp_index, sent_arr in enumerate(arr):
            sent_index = np.argmax(sent_arr)
            if not sent_index == 0:
                sentiment = self.class_index[sent_index]
                aspect = self.aspect_index[asp_index]
                result.append((aspect, sentiment))

        return result
