import os
import json

file_path = os.path.abspath(__file__)
splitted = file_path.split('/')

root_splitted = splitted[:-3]
ROOT = ''
for dir in root_splitted:
    ROOT += dir + '/'

def load_translator_register():
    with open(ROOT + '/Resources/Register/translator_register.json', 'r') as f:
        index = json.load(f)

    return index


def load_word_vector_builder_register():
    with open(ROOT + '/Resources/Register/word_vector_builder_register.json', 'r') as f:
        index = json.load(f)

    return index


def load_embedding_register():
    with open(ROOT + '/Resources/Register/embedding_register.json', 'r') as f:
        index = json.load(f)

    return index


def load_str_from_txt(path):
    with open(path, 'r') as f:
        return f.read()


def load_train_config(path):
    with open(path + '/train_config.json', 'r') as f:
        config = json.load(f)

    return config


def load_data_preparation_config(path):
    with open(path + 'data_preparation_config.json', 'r') as f:
        config = json.load(f)

    return config


def load_validation_config():
    with open(ROOT + '/Resources/Config/validation_config.json', 'r') as f:
        config = json.load(f)

    return config


def load_index(path):
    with open(path, 'r') as wi:
        return json.load(wi)