import json
import os


def save_to_json(_dict, path):
    check_path_existence(path)

    with open(path, 'w') as file:
        json.dump(_dict, file)


def save_string_to_txt(text, path):
    check_path_existence(path)

    with open(path, 'w') as file:
        file.write(text)
        file.close()


def check_path_existence(path):
    if len(path.split('.')) > 1:
        path_without_file = path.split('/')[:-1]
        temp = ''
        for elem in path_without_file:
            temp += elem + '/'
        path_without_file = temp
        if not os.path.exists(path_without_file):
            os.makedirs(path_without_file)
    else:
        if not os.path.exists(path):
            os.makedirs(path)