from xml.etree import ElementTree as ET

class XMLTranslatorGermEval:
    def __init__(self, path):
        self.path = path


    def translate(self):
        tree = ET.parse(self.path)
        root = tree.getroot()

        result = []

        for document in root:
            id = document.attrib['id']
            text = ''
            aspects = []
            sentiment = ''
            relevance = True

            for entity in document:
                if (entity.tag == 'sentiment'):
                    sentiment = entity.text

                if (entity.tag == 'text'):
                    text = entity.text

                if (entity.tag == 'relevance'):
                    relevance = entity.text

                if (entity.tag == 'Opinions'):
                    aspects = []
                    for opinion in entity:
                        aspect = opinion.attrib['category']
                        main_aspect = aspect.split('#')[0]
                        aspect_sentiment = opinion.attrib['polarity']
                        if not (main_aspect, aspect_sentiment) in aspects:
                            aspects.append((main_aspect, aspect_sentiment))

            data = {'ids': id,
                    'texts': text,
                    'aspect_sentiments': aspects,
                    }

            result.append(data)

        return result

