from xml.etree import ElementTree as ET

class XMLTranslatorSemEval:

    def __init__(self, path):
        self.path = path

    def translate(self):
        tree = ET.parse(self.path)
        root = tree.getroot()

        result = []

        for review in root:
            try:
                id = review.attrib['rid']
            except:
                id = 'not found'

            text = ''
            aspects = []
            opinion_for_sentence = False
            for sentences in review:
                for sentence in sentences:
                    for entity in sentence:
                        if (entity.tag == 'text'):
                            text += entity.text + ' '
                            continue

                        if (entity.tag == 'Opinions'):
                            for opinion in entity:
                                aspect = opinion.attrib['category']
                                main_aspect = aspect.split('#')[0]
                                aspect_sentiment = opinion.attrib['polarity']
                                if not (main_aspect, aspect_sentiment) in aspects:
                                    aspects.append((main_aspect, aspect_sentiment))
                                opinion_for_sentence = True
                                continue

                if not opinion_for_sentence:
                    if sentences.tag == 'Opinions':
                        for opinion in sentences:
                            aspect = opinion.attrib['category']
                            main_aspect = aspect.split('#')[0]
                            aspect_sentiment = opinion.attrib['polarity']
                            if not (main_aspect, aspect_sentiment) in aspects:
                                aspects.append((main_aspect, aspect_sentiment))
                            continue

            data = {'ids': id,
                    'texts': text,
                    'aspect_sentiments': aspects,
                    }

            result.append(data)

        return result