from Core.IO.load import load_str_from_txt

class TXTTranslator:

    def __init__(self, path):
        self.path = path


    def translate(self):
        texts = load_str_from_txt(self.path)

        texts = texts.split('\n')
        result = []

        for text in texts:
            result.append({'ids': None,
                           'texts': text,
                           'aspect_sentiments': None})

        return result