import importlib

from Core.DataLoading.DataRepresentation import DataRepresentation
from Core.IO.load import load_translator_register


class DataProvider:

    def __init__(self, source, format):
        '''
        This class translates the data in the given source to the intern schema
        :param source: Path to a file or URL of REST API / DB
        :param format: Format of the given source. Can XML, JSON, etc.
        :param structure: String that defines the structure of the source
        '''
        self.source = source
        self.format = format
        self.translator = self._build_translator()

    def get_data(self):
        data = self.translator.translate()
        data = self._dict_list_to_list_dict(data)

        return DataRepresentation(**data)

    def _dict_list_to_list_dict(self, dicts):
        keys = list(dicts[0].keys())
        result = {}

        for key in keys:
            result[key] = []

        for dict in dicts:
            for key in keys:
                result[key].append(dict[key])

        return result

    def _build_translator(self):
        translator_index = load_translator_register()
        try:
            translator_meta = translator_index[self.format]
        except:
            raise ValueError('format must be one of the following: ' + str(list(translator_index.keys())) + '. Given value is: ' + self.format)
        module = importlib.import_module(translator_meta['module'])
        return getattr(module, translator_meta['class'])(self.source)

