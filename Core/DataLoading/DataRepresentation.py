import numpy as np

class DataRepresentation:
    def __init__(self, ids=None, texts=None, aspect_sentiments=None, polarities='not_defined'):
        self.ids = ids
        self.texts = texts
        self.polarities = polarities
        self.aspect_sentiments = aspect_sentiments
        self._check_for_conflicts()

    def getText(self):
        return self.texts

    def getSentiment(self):
        return self.polarities

    def getAspectSentiments(self):
        return self.aspect_sentiments

    def _check_for_conflicts(self):

        all_text_labels = []
        for asp_sent in self.aspect_sentiments:
            aspects = {}
            if asp_sent == None:
                continue
            for aspect, sentiment in asp_sent:
                if not aspect in aspects:
                    aspects[aspect] = [sentiment]
                else:
                    aspects[aspect].append(sentiment)

            for asp in aspects:
                sentiments = aspects[asp]
                unique_sents = np.unique(np.array(sentiments))

                if 'positive' in unique_sents and 'negative' in unique_sents:
                    aspects[asp] = ['conflict']
                if 'neutral' in unique_sents:
                    if 'positive' in unique_sents:
                        aspects[asp] = ['positive']
                    if 'negative' in unique_sents:
                        aspects[asp] = ['negative']

            one_text_label = []
            for asp in aspects:
                sentiments = aspects[asp]
                for sentiment in sentiments:
                    one_text_label.append((asp, sentiment))

            all_text_labels.append(one_text_label)
        self.aspect_sentiments = all_text_labels
